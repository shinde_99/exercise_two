
function flatten(cars,helper)
{     
     for (let i=0;i<cars.length;i++)
    {
      var element=cars[i];
      if(Array.isArray(element))
      {
        flatten(element,helper);
      }
      else
      {
        helper(element)
      }
    }
    
  }
  
  
module.exports=flatten;