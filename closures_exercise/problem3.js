function cacheFunction(cb)
{
    let temp={};
    return function xyz()
    {
        let arg = [...arguments].join(',')
        if(arg in temp)
        {
            return temp(arg)
        }
        else
        {
            temp[arg]=cb(...arguments)
            return temp[arg]
        }
    }
        
}
module.exports=cacheFunction;
