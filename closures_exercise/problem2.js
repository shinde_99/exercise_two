function limitFunctionCallCount(callback, n) 
{     var count = 0;
      function inner() 
      {
          count++;
          if(count<=n)
          {
              return callback;
          }
          else
          {
              return null;
          }
      }
      return inner;
      
}

module.exports = limitFunctionCallCount;