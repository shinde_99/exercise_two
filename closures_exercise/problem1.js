function counterFactory() 
{
    var count = 10;
    var testObject = 
    {
        "increment" : ()
        {
            count++;
            console.log(count);
        },
        "decrement" : function()
        {
            count--;
            console.log(count);
        }
    };
    return(testObject);
}
module.exports = counterFactory;